
(ns sample.primality
  ;(:require )
  ;(:use )
  ;(:import )
  )

; Structure and Interpretation of Computer Programs
; Section 1.2 Procedures and the Processes They Generate
; Subsection 1.2.6 Example: Testing for Primality

; Exercise 1.22: solution
; Description: Find using search-for-primes if the order of
;              growth is theta(root(n))

(defn square
  [x]
  (* x x))

(defn divides?
  [a b]
  (= (mod b a) 0))

(defn find-divisor
  [n test-divisor]
  (loop [n n test-divisor test-divisor]
      (cond
        (> (square test-divisor) n) n
        (divides? test-divisor n) test-divisor
        :else (recur n (+ test-divisor 1)))))

(defn smallest-divisor
  [n]
  (find-divisor n 2))

(defn prime?
  [n]
  (= n (smallest-divisor n)))

(defn report-prime
  [elapsed-time]
  (print " *** ")
  (print elapsed-time))

(defn start-prime-test
  [n start-time]
  (if (prime? n)
    (report-prime (- (System/currentTimeMillis) start-time))))

(defn timed-prime-test
  [n]
  (newline)
  (print n)
  (start-prime-test n (System/currentTimeMillis)))


(defn search-for-primes
  [min]
  (loop [n min remaining 3]
    (cond
      (= remaining 0) (newline)
      (prime? n) (do
                   (timed-prime-test n)
                   (recur (+ 2 n) (- remaining 1)))
      :else (recur (+ 2 n) remaining))))

; tests
;(search-for-primes 1001)
;(search-for-primes 10001)
;(search-for-primes 100001)
;(search-for-primes 1000001)
;(search-for-primes 10000001)
;(search-for-primes 100000001)
;(search-for-primes 1000000001)
;(search-for-primes 10000000001)
;(search-for-primes 100000000001)
;(search-for-primes 1000000000001)

; Exercise 1.23: solution
; Description: Change find-divisor and see if it's
;              twice as fast (it almost is but not *2 cause
;              of fewwer iterations)

(defn nxt
  [n]
  (if (= n 2) 3 (+ n 2)))

(defn find-divisor-v2
  [n test-divisor]
  (loop [n n test-divisor test-divisor]
      (cond
        (> (square test-divisor) n) n
        (divides? test-divisor n) test-divisor
        :else (recur n (nxt test-divisor)))))

(defn smallest-divisor-v2
  [n]
  (find-divisor-v2 n 2))

; Exercise 1.24: solution
; Description: Repeat exercise 1.22 using fast-prime?
;              in timed-prime-test and test the times.

(defn expmod
  [base exp m]
  (cond
    (= exp 0) 1
    (even? exp) (mod (square (expmod base (/ exp 2) m)) m)
    :else (mod (* base (expmod base (- exp 1) m)) m)))

(defn fermat-test
  [n]
  (defn try-it
    [a]
    (= (expmod a n n)))
  (try-it (+ 1 (rand-int (- n 1)))))

(defn fast-prime?
  [n times]
  (loop [n n times times]
    (cond
      (= times 0) true
      (fermat-test n) (recur n (- times 1))
      :else false)))

(defn prime-v2? [n] (fast-prime? n 4))

(defn start-prime-test-v2
  [n start-time]
  (if (prime-v2? n)
    (report-prime (- (System/currentTimeMillis) start-time))))

(defn timed-prime-test-v2
  [n]
  (newline)
  (print n)
  (start-prime-test-v2 n (System/currentTimeMillis)))

(defn search-for-primes-v2
  [min]
  (loop [n min remaining 3]
    (cond
      (= remaining 0) (newline)
      (prime? n) (do
                   (timed-prime-test-v2 n)
                   (recur (+ 2 n) (- remaining 1)))
      :else (recur (+ 2 n) remaining))))

; tests
;(println "search-for-primes-v2")
;(search-for-primes-v2 1001)
;(search-for-primes-v2 10001)
;(search-for-primes-v2 100001)
;(search-for-primes-v2 1000001)
;(search-for-primes-v2 10000001)
;(search-for-primes-v2 100000001)
;(search-for-primes-v2 1000000001)
;(search-for-primes-v2 10000000001)
;(search-for-primes-v2 100000000001)
;(search-for-primes-v2 1000000000001)

; Exercise 1.27: solution
; Description: Fool fermi test with Carmichael numbers

(defn test-carmichael
  [n]
  (loop [a 1]
    (cond
      (= n a) true
      (= (expmod a n n) (mod a n)) (recur (+ 1 a))
      :else false)))

(println "Fool fermi test with Carmichael numbers")
(when (test-carmichael 561) (println "561 is prime"))
(when (test-carmichael 1105) (println "1105 is prime"))
(when (test-carmichael 1729) (println "1729 is prime"))
(when (test-carmichael 2465) (println "2465 is prime"))
(when (test-carmichael 6601) (println "6601 is prime"))
