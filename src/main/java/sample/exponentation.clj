
(ns sample.exponentation
  ;(:require )
  ;(:use )
  ;(:import )
  )

; Structure and Interpretation of Computer Programs
; Section 1.2 Procedures and the Processes They Generate
; Subsection 1.2.4 Exponentiation

; Exercise 1.16: solution
; Description: Compute b^n in iterative fashion.

(defn fast-expt
  [b n]
  (loop [a 1 b b n n]
    (cond
      (= n 0) a
      (even? n) (recur a (* b b) (/ n 2))
      :else (recur (* a b) b (- n 1)))))

(println "fast-expt test")
(println (fast-expt 2 0))
(println (fast-expt 2 1))
(println (fast-expt 2 2))
(println (fast-expt 2 100))

; Exercise 1.17: solution
; Description: Given doubled and halved define a * function.

(defn epi
  [a b]
  (defn doubled
    [x]
    (* 2 x))
  (defn halved
    [x]
    (when (even? x) (/ x 2)))
  (cond
    (= b 0) 0
    (even? b) (epi (doubled a) (halved b))
    :else (+ a (epi a (- b 1)))))

(println "epi test")
(println (epi 2 0))
(println (epi 2 4))
(println (epi 30 121))

; Exercise 1.18: solution
; Description: Given doubled and halved define a * function
;              that uses a logarithmic number fo steps.

(defn fast-epi
  [a b]
  (defn doubled
    [x]
    (* 2 x))
  (defn halved
    [x]
    (when (even? x) (/ x 2)))
  (loop [s 0 a a b b]
    (cond
      (= b 0) s
      (even? b) (recur s (doubled a) (halved b))
      :else (recur (+ s a) a (- b 1)))))

(println "fast-epi test")
(println (fast-epi 2 0))
(println (fast-epi 2 4))
(println (fast-epi 30 121))

; Exercise 1.19: solution
; Description: Compute nth Fibonacci number efficiently.
;              Starting with (a0, b0), and applying Tpq once
;              we get (qb0 + qa0 + pa0, pb0 + qa0),
;              or a1 = qb0 + qa0 + pa0, b1 = pb0 + qa0.
;              Applying Tpq a second time we get
;              (qb1 + qa1 + pa1, pb1 + qa1),
;              or a2 = b0(pq + qq + pq) + a0(qq + qq + pq + pq + pp),
;              b2 = b0(pp + qq) + a0(pq + qq + pq)
;              We want a2 = q’b0 + q’a0 + p’a0, b2 = p’b0 + q’a0
;              therefore if we take p’ = pp + qq, and q’ = 2pq + qq,
;              it works out in one transformation.

(defn fib [n]
  (defn fib-iter [a b p q count]
    (loop [a a b b p p q q count count]
      (cond (= count 0) b
        (even? count)
        (recur
          a
          b
          (+ (* p p) (* q q))    ; compute p'
          (+ (* 2 p q) (* q q))  ; compute q'
          (/ count 2))
        :else (recur
                (+ (* b q) (* a q) (* a p))
                (+ (* b p) (* a q))
                p
                q
                (- count 1)))))
  (fib-iter 1 0 0 1 n))

(println "fib test")
(println (fib 100))