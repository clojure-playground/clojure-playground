
(ns sample.core)

(defn average
  [x y]
  (/ (+ x y) 2))

(defn positive? [x] (if (< x 0) false true))
(defn negative? [x] (not (positive? x)))

(defn close-enough?
  [x y]
  (< ((fn [x] (if (> x 0) x (- x))) (- x y)) 0.001))
  ;(< (. Math abs (- x y)) 0.001))

(defn search
  [f neg-point pos-point]
  (let [midpoint (average neg-point pos-point)]
    (if (close-enough? neg-point pos-point)
      midpoint
      (let [test-value (f midpoint)]
        (cond
          (positive? test-value) (search f neg-point midpoint)
          (negative? test-value) (search f midpoint pos-point)
          :else midpoint)))))

(defn half-interval-method
  [f a b]
  (let [a-value (f a)
        b-value (f b)]
    (cond
      (and (negative? a-value) (positive? b-value)) (search f a b)
      (and (negative? b-value) (positive? a-value)) (search f b a)
      :else (println "Error: Values are not of opposite sign" a b))))

(println "PI approximation using halv-interval-method:")
(println (half-interval-method (fn [x] (. Math sin x)) 2.0 4.0))
(println "Search for a root of \"x^3 - 2*x -3 = 0\":")
(println (half-interval-method (fn [x] (- (* x x x) (* 2 x) 3)) 1.0 2.0))

(def tolerance 0.00001)
(defn fixed-point
  [f first-guess]
  (defn close-enough-v2?
    [v1 v2]
    (< ((fn [x] (. Math abs x)) (- v1 v2)) tolerance))
  (defn try
    [guess]
    (let [next (f guess)]
      (if (close-enough-v2? guess next)
        next
        (try next))))
  (try first-guess))
(defn mcos [x] (. Math cos x))
(fixed-point mcos 1.0)