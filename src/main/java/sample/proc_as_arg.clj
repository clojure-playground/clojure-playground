
(ns sample.proc-as-arg
  ;(:require )
  ;(:use )
  ;(:import )
  )



; Structure and Interpretation of Computer Programs
; Exercise 1.29: solution
; Description: Numerical integration using Simpson's
;              Rule method.

; h = (b - a) / n for even integer n
; yk = f(a + kh)
(defn coef
  [k n]
  (cond
    (= k 0) 1
    (= k n) 1
    (even? k) 2
    :else 4))

(defn sum-simp
  [f a h n]
  (loop [k 0 acc 0]
    (if (> k n)
      acc
      (recur (inc k) (+ acc (* (coef k n) (f (+ a (* k h)))))))))

(defn integral
  [f a b n]
  (let [h (/ (- b a) n)]
    (/ (* h (sum-simp f a h n)) 3)))

(defn cube [x] (* x x x))

; test (= 0.25)
(println (float (integral cube 0 1 2)))

; Structure and Interpretation of Computer Programs
; Exercise 1.30: solution
; Description: Make procedure sum iterative.

(defn sum
  [term a next b]
  (loop [a a result 0]
    (if (> a b)
      result
      (recur (next a) (+ result (term a))))))

(defn sum-int
  [a b]
  (sum (fn [x] x) a inc b))

; test (= 55)
(println (sum-int 1 10))


; Structure and Interpretation of Computer Programs
; Exercise 1.31: solution
; Description: Write a procedure that returns the product
;              values of a function at points over a given
;              range.

(defn product
  [term a next b]
  ; iterative
  (loop [a a result 1.0]
    (cond
      (= a 0) 0
      (> a b) result
      :else (recur (next a) (* result (term a))))))

(defn factorial
  [n]
  (product (fn [x] x) 1 inc n))

(println "factorial of 5:" (factorial 5))


(defn pi-product
  [a b]
  (defn pi-term [x]
    (cond
      (= x 1) (/ 2 3)
      (even? x) (/ (+ x 2) (+ x 1))
      :else (/ (+ x 1) (+ x 2))))
  (product pi-term a inc b))

(println "Approximation of pi:"(* 4 (pi-product 1 1000)))

(defn product-rec
  [term a next b]
  ; recursive
  (if (> a b)
    1
    (* (term a) (product-rec term (next a) next b))))

; Structure and Interpretation of Computer Programs
; Exercise 1.32: solution
; Description: Write an abstraction procedure to sum
;              and product. Rewrite product using it
;              and if accumulate is iterative rewrite
;              it to recursive and vice versa

(defn accumulate
  [combiner null-value term a next b]
  ; iterative
  (loop [a a result null-value]
    (if (> a b)
      result
      (recur (next a) (combiner result (term a))))))

(defn product-accu
  [term a next b]
  (accumulate * 1.0 term a next b))

(defn accumulate-rec
  [combiner null-value term a next b]
  ; recursive
  (if (> a b)
    null-value
    (combiner a
      (accumulate-rec combiner null-value term (next a) next b))))

; Structure and Interpretation of Computer Programs
; Exercise 1.32: solution
; Description: Rewrite a more general accumulate using
;              a filter

(defn filtered-accumulate
  [predicate combiner null-value term a next b]
  (loop [a a result null-value]
    (cond
      (> a b) result
      (predicate a) (recur (next a) (combiner result (term a)))
      :else (recur (next a) result))))

(defn square [x] (* x x))

(defn divides?
  [a b]
  (= (mod b a) 0))

(defn find-divisor
  [n test-divisor]
  (loop [n n test-divisor test-divisor]
      (cond
        (> (square test-divisor) n) n
        (divides? test-divisor n) test-divisor
        :else (recur n (+ test-divisor 1)))))

(defn smallest-divisor
  [n]
  (find-divisor n 2))

(defn prime?
  [n]
  (= n (smallest-divisor n)))

(defn prime-square-sum
  [a b]
  (filtered-accumulate prime? + 0 square a inc b))

(println "Prime square sum:" (prime-square-sum 1 6))

(defn relative-prime-product
  [n]
  (defn gcd
    [a b]
    (if (= b 0)
      a
      (gcd b (mod a b))))
  (defn filtera
    [i]
    (= (gcd i n) 1))
  (filtered-accumulate filtera * 1 (fn [x] x) 1 inc (dec n)))

(println "Relative prime product (n=9):" (relative-prime-product 9))
  