
(ns sample.roots
  ;(:require )
  ;(:use )
  ;(:import )
  )


(ns sample.cube-root)

; Structure and Interpretation of Computer Programs
; Exercise 1.7: solution
; Description: Computes the cube root of a number

; Version 1
;(defn square
;  "Returns the square of x."
;  [x]
;  (* x x))

;(defn cube
;  "Returns the cube of x."
;  [x]
;  (* x x x))

;(defn abs
;  "Returns the absolute value of x."
;  [x]
;  (if (< x 0) (- x) x))

;(defn improve
;  "Returns a new guess."
;  [guess x]
;  (/ (+ (/ x (square guess)) (* 2 guess)) 3))

;(defn good-enough?
;  "Find according to the previous guess if the current 1 is the
;  result and returns it."
;  [guess prev-guess]
;  (< (/ (abs (- guess prev-guess)) guess) 0.001))

;(defn cbrt-iter
;  "Recursive find of a good number closed to cube-root of x"
;  [guess prev-guess x]
;  (if (good-enough? guess prev-guess)
;    guess
;    (cbrt-iter (improve guess x) guess x)))

;(defn cbrt
;  "Returns the cube-root of a number."
;  [x]
;  (cbrt-iter 2.0 1.0 x))

; Version 2
(defn cbrt-v2
  "Returns the cube-root of a number."
  [x]
  ; This implementation uses lexical scoping
  (defn square [num]
    (* num num))
  (defn cube []
    (* x x x))
  (defn abs [num]
    (if (< num 0) (- num) num))
  (defn improve [guess]
    (/ (+ (/ x (square guess)) (* 2 guess)) 3))
  (defn good-enough? [guess prev-guess]
    (< (/ (abs (- guess prev-guess)) guess) 0.001))
  (defn cbrt-iter [guess prev-guess]
    (if (good-enough? guess prev-guess)
        guess
        (cbrt-iter (improve guess) guess)))
  (cbrt-iter 2.0 1.0))

; Tests
;(println (cbrt-v2 27))
;(println (cbrt-v2 2))
;(println (cbrt-v2 0.001))
;(println (cbrt-v2 0.000001))
;(println (cbrt-v2 0.000000000001))

(ns sample.square-root
  ;(:require )
  ;(:use )
  ;(:import )
  )

; Structure and Interpretation of Computer Programs
; Exercise 1.6: solution
; Description: Computes the square root of a number using
;              2 different methods of aproximation.

(defn square [x]
  (* x x))

(defn abs [x]
  (if (< x 0) (- x) x))

(defn average [x y]
  (/ (+ x y) 2))

(defn improve [guess x]
  (average guess (/ x guess)))

(defn good-enough? [guess x]
  (< (abs (- (square guess) x)) 0.001))


(defn sqrt-iter [guess x]
  (if (good-enough? guess x)
    guess
    (sqrt-iter (improve guess x) x)))

(defn sqrt [x]
  (sqrt-iter 1.0 x))

;version 2
(defn good-enough-v2? [guess prev-guess]
  (< (/ (abs (- guess prev-guess)) guess) 0.001))

(defn sqrt-iter-v2 [guess prev-guess x]
  (if (good-enough-v2? guess prev-guess)
    guess
    (sqrt-iter-v2 (improve guess x) guess x)))

(defn sqrt-v2 [x]
  (sqrt-iter-v2 2.0 1.0 x))

; Tests
(println (sqrt 2))
(println (sqrt 0.001))
(println (sqrt 0.000001))
(println (sqrt 0.000000000001))
(println "===============================")
(println (sqrt-v2 2))
(println (sqrt-v2 0.001))
(println (sqrt-v2 0.000001))
(println (sqrt-v2 0.000000000001))
